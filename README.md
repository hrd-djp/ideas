HRD-DJP Ideas
==============

Repository ini merupakan issue queue untuk penyampaian feature request maupun bug report dari Public atas Sistem Informasi SDM DJP.

Daftar Pelaporan yang sudah masuk dapat dilihat [disini](https://gitlab.com/hrd-djp/ideas/issues).

## Lingkup Aplikasi
Berikut merupakan daftar aplikasi yang issue-nya dapat dilaporkan ke repository ini:

* [SIKKA](https://sikka-djp/)
  | Aplikasi Manajemen SDM DJP
* [SIKKA WS](https://sikka-djp/)
  | Web Servis SIKKA untuk aplikasi mitra yang menggunakan data SIKKA
* [Digitalisasi](https://sikka-djp/)
  | Aplikasi Digitalisasi Dokumen Kepegawaian DJP
* [Sinkronisasi](https://sikka-djp/)
  | Untuk sinkronisasi data pegawai DJP dengan Kustodian dan Server Absensi KPDJP

# Metode Pelaporan

Untuk dapat membuat pelaporan, Anda harus login terlebih dahulu ke aplikasi gitlab.com, silakan membuat akun baru atau login menggunakan akun Google.

## Bug Report

Sebelum membuat pelaporan terkait Bug, pastikan bahwa:
1. Kejadian/ permasalahan tersebut dapat di-reproduce (terjadi lebih dari satu kali).
1. Anda sudah menyimpan screenshot atau foto dari permasalahan tersebut.

Jika permasalahan masih terjadi, silakan kirimkan issue melalui [link ini](https://gitlab.com/hrd-djp/ideas/issues/new) dengan format [seperti ini](https://gitlab.com/hrd-djp/ideas/raw/master/docs/bug.md).

## Feature Request

Sebelum membuat pelaporan terkait penambahan fitur, pastikan bahwa:
1. Fitur ini akan memberikan tambahan manfaat untuk pegawai DJP/ manajemen DJP dalam mengelola pegawai.
1. Fitur belum tersedia di SIKKA.

Untuk pengajuan penambahan fitur bisa dilakukan di [link ini](https://gitlab.com/hrd-djp/ideas/issues/new) dengan format [seperti ini](https://gitlab.com/hrd-djp/ideas/raw/master/docs/feature.md).

# FAQ
*  Dimanakah repository/ script Aplikasi SIKKA? Apakah kami bisa melihatnya?

Mohon maaf untuk script SIKKA tidak di publish dan bersifat closed source. Hanya tim programmer yang ditugaskan untuk mengembangkan Aplikasi Manajemen SDM yang dapat melihatnya.

*  Saya ingin berkontribusi dalam pengembangan aplikasi. Adakah yang bisa saya lakukan?

Tentu, pertama-tama silakan berkontribusi melalui [daftar pelaporan](https://gitlab.com/hrd-djp/ideas/issues) yang ada, Anda dapat membantu membuatkan gambaran logic atas feature request ataupun bug report.

*  Apakah kami dapat meminta follow up atas bug report atau feature request yang sudah diajukan?

Untuk pelaporan yang sudah masuk, kami akan memprosesnya ke repository development. Apabila pengembangan/perbaikan sudah selesai, kami akan follow up pada issue yang sudah dilaporkan.